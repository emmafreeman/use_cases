# Segment users into use cases

Pulls yesterday's orders data from Redshift, categorizes each order into one or more use cases, and stores back to Redshift table '_order.use_cases'