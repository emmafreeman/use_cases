"""
January 2019
Emma Freeman

Parses and cleans the message text, classify orders into use cases
Saves csv to S3 and copies to Redshift _order.use_cases table
"""

# import packages
import re
import pandas as pd
import json
import configparser
import psycopg2
import boto3
from io import StringIO
import datetime
import os, sys

def connect_to_db():
    """
    establishes a connection to the database using psycopg2
    """
    
    config = configparser.ConfigParser(allow_no_value=True, interpolation=None)
    # setting interpolation to None disables interpreting certain characters like '%' as string formatting
    config.read('config')
    
    # get database connection credentials
    dbname = config['redshift']['database']
    user = config['redshift']['user']
    password = config['redshift']['password']
    host = config['redshift']['host']
    port = config['redshift']['port']
    
    conn = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port
            )
    cur = conn.cursor()

    return conn, cur
    


def query_to_df():
    """
    runs a query on the database and saves the results to a dataframe
    """
    query = """
        SELECT 
            distinct orders.id  AS "order_id",
            op.messages AS "message",
            listagg(distinct "tag", ', ')  AS "photo_tags_list"

        FROM legacy.orders  AS orders
        LEFT JOIN legacy.dispatch_lines  AS dispatch_lines ON dispatch_lines.order_id=orders.id 
        LEFT JOIN legacy.jobs  AS jobs ON dispatch_lines.id = jobs.dispatch_line_id 
        LEFT JOIN legacy.users  AS users ON orders.user_id=users.id 
        LEFT JOIN photo_tags.photo_tags AS photo_tags ON orders.id=photo_tags.order_id
        LEFT JOIN commerce.shipment_items  AS si ON si.ordered_product_shipment_id = jobs.id 
        LEFT JOIN _order.ordered_products_view_geo AS op ON si.ordered_product_id=op.ordered_product_id 

        WHERE date_trunc('day', timestamp 'epoch' + orders.created  * interval '1 second') = (current_date - interval '1 day')
        AND op.messages is not null
        AND dispatch_lines.dispatch_line_status_id != 6
        AND users.email not like '%touchnote%'
        AND jobs.product_type  IN ('PC', 'GC')
        AND users.country_id in (1, 37)
        AND (photo_tags.probability > 0.88 OR photo_tags.probability is null)

        GROUP BY 1,2
        ;
        """
    conn, cur = connect_to_db()
    cur.execute(query)
    results = cur.fetchall()
    df = pd.DataFrame(results, columns=["order_id", "message", "photo_tags"])
    cur.close()
    conn.close()
    return df


def parse_message_text(text):
    """
    parses the text of the messages from orders
    """
    message = ''
    
    if text is not None:
        try:
            s = json.loads(text)
            for chunk in s:
                try:
                    message += chunk['text'] + ' '
                except:
                    message = message
            message = message.replace('\n', ' ').lower()
            message = re.sub(r"\bu\b", "you", message)
        except:
            message = message
    else:
        message = None
    
    return message


def baby_words_search(text):
    """
    uses regex to define search for baby type words in messages
    """
    
    regex = re.compile("([0-9]+ *)(lbs|pounds) *([0-9]+ *)(oz|ounces) *|(born on)|(?<!in )([0-9]+) (month)+|crawling|weeks old")
    unregex = re.compile("passed away|memorial service")
    if regex.search(text) and not unregex.search(text):
        return 1
    else:
        return 0


def classify_use_cases(df):
    """
    classifies each order into one or more use cases based on messages and photo tags
    returns a df
    """

    # define regex for use cases
    travel_words = re.compile("(?<!you are )(?<!you're )(?<!your )((having (a|an) [a-z ]+|our )" +
                              "(time|trip|holiday|vacation|week in))|(hello|greetings|hi|aloha|hallo) " +
                              "from |(we|i) (visited|toured|saw the)|took a trip|went to the"
                              "|on holiday|sightseeing|beach|swimming|island|scenery|view from|views|hiked" +
                              "|(our |my )(hotel|trip|vacation|holiday)|the (hotel|resort)|(i|we) " +
                              "(stayed|are staying) in")
    bday_words = re.compile("(happy |your |(have (a|an) [a-z ]+)+)([0-9a-z]*)(th |nd |st )" + 
                            "*(?<!my )(birthday|bday|anniversary)") 
    thank_you_words = re.compile("(thanks|thank you|thankyou) [a-z ]*for")
    misc_holidays = re.compile("(season'*s'* greetings)|(happy |merry |have (a|an) [a-z ]+)+(father'*s'* day|mother'*s'* day|halloween|thanksgiving|new year'*s'*|valentine'*s'*|easter|christmas|xmas|holidays|holiday season|hanukkah|chanukah|hanukah|hannukah|chanuka|chanukkah|hanuka|channukah|chanukka|hanukka|hannuka|hannukkah|channuka|xanuka|hannukka|channukkah|channukka|chanuqa)+")
    
    df['travelling'] = df['clean_message'].apply(lambda x: 1 if re.search(travel_words, x) else 0)
    df['birthday'] = df['clean_message'].apply(lambda x: 1 if re.search(bday_words, x) else 0)
    df['thank_you'] = df['clean_message'].apply(lambda x: 1 if re.search(thank_you_words, x) else 0)
    df['holidays'] = df['clean_message'].apply(lambda x: 1 if re.search(misc_holidays, x) else 0)
    df['baby_words'] = df['clean_message'].apply(baby_words_search)
    df['baby_pic'] = df[pd.notnull(df['photo_tags'])]['photo_tags'].apply(lambda x: 1 if any(word in x 
                                                                                 for word in ['baby', 'toddler']) else 0)
    df['parenting'] = df.apply(lambda x: 1 if (x['baby_words']==1) or (x['baby_pic']==1) else 0, axis=1)

    labels = ['travelling', 'birthday', 'thank_you', 'holidays', 'parenting']

    df['count_use_cases'] = df[labels].sum(axis=1)

    df['just_because'] = df.apply(lambda x: 1 if x['count_use_cases']==0 else 0, axis=1)

    df.drop(['baby_words', 'baby_pic', 'count_use_cases', 'message'], axis=1)

    return df


def get_use_cases_as_list(df):
    """
    combine all the use cases for each order into a single column as a list
    """
    
    df.dropna(inplace=True, subset=['message'])
    use_cases = ['travelling', 'birthday', 'thank_you', 'holidays', 'parenting', 'just_because']
    df['count_use_cases'] = df[use_cases].sum(axis=1)
    
    for uc in use_cases:
        df[uc] = df[uc].apply(lambda x: uc if x==1 else '')
        
    df['use_cases'] = df[use_cases].apply(
        lambda x: ', '.join(x).strip(', ').replace(', , ,', ', ').replace(' , ', ' '), axis=1)
    
    df_final = df.loc[:, ['order_id', 'use_cases', 'count_use_cases']]

    return df_final


def get_final_df():
    """
    applies all of the above functions and returns a clean dataframe and filename with current timestamp
    """
    df = query_to_df()
    
    df['clean_message'] = df['message'].apply(parse_message_text)

    orders_classified = classify_use_cases(df)

    df_final = get_use_cases_as_list(orders_classified)

    df_final.drop_duplicates(inplace=True)
    
    current_datetime = datetime.datetime.now().isoformat()[0:-7]
    filename = "use_cases_tagged_" + current_datetime + ".csv"

    return df_final, filename


def s3_creds():
    """
    gets credentials for s3
    """
    config = configparser.ConfigParser(allow_no_value=True,interpolation=None)
    config.read('config')
    access_key = config['s3']['access_key']
    secret_key = config['s3']['secret_key']
    return access_key, secret_key


def connect_to_s3():
    """
    establishes connection to S3 bucket using boto3
    """
    access_key, secret_key = s3_creds()
    
    s3 = boto3.resource(
    's3',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key
    )
    
    return s3


def df_to_s3(df, filename, bucket_name='tn.retention'):
    """
    saves the df to a csv and stores in s3 using boto3
    """
    csv_buffer = StringIO()
    df.to_csv(csv_buffer, index=False)

    # store csv in S3
    s3 = connect_to_s3()
    s3.Object(bucket_name, filename).put(Body=csv_buffer.getvalue())


def s3_to_db(filename, bucket_name='tn.retention', region_name = 'us-east-2'):
    """
    copies the csv from S3 to Redshift using psycopg2
    """
    access_key, secret_key = s3_creds()
    
    query = f"""
        copy _order.use_cases 
        from 's3://{bucket_name}/{filename}'
        delimiter ','
        ignoreheader as 1
        removequotes
        access_key_id '{access_key}'
        secret_access_key '{secret_key}'
        region '{region_name}'
        ;
        """
    conn, cur = connect_to_db()
    cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()

df = query_to_df()

df, filename = get_final_df()

df_to_s3(df, filename)

s3_to_db(filename)
