"""
January 2019
Emma Freeman

Calls functions in use_case_funcs.py
"""

# import packages
import configparser
import re
import pandas as pd
import psycopg2
import numpy as np
import json
import boto3
from io import StringIO
import datetime
import os, sys

from use_cases_funcs import get_final_df, df_to_s3, s3_to_db 

df, filename = get_final_df()

df_to_s3(df, filename)

s3_to_db(filename)