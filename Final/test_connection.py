"""
updated 2019-01-29
January 2019
Emma Freeman

tests connection to redshift
"""

# import packages
import configparser
import psycopg2


try:
    config = configparser.ConfigParser(allow_no_value=True, interpolation=None)
    # setting interpolation to None disables interpreting certain characters like '%' as string formatting
    config.read('config')

    # get database connection credentials
    dbname = config['redshift']['database']
    user = config['redshift']['user']
    password = config['redshift']['password']
    host = config['redshift']['host']
    port = config['redshift']['port']
    print(f"dbname from config is {dbname}")
    conn = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port
            )
    cur = conn.cursor()

    query = "select * from legacy.orders limit 5"
    cur.execute(query)
    print(cur.fetchall())
    cur.close()
    conn.close()

except:
    print("can't connect")
